import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.orekit.bodies.CelestialBody;
import org.orekit.bodies.CelestialBodyFactory;
import org.orekit.errors.OrekitException;
import org.orekit.forces.gravity.potential.GravityFieldFactory;
import org.orekit.forces.gravity.potential.UnnormalizedSphericalHarmonicsProvider;
import org.orekit.frames.Frame;
import org.orekit.propagation.semianalytical.dsst.forces.DSSTForceModel;
import org.orekit.propagation.semianalytical.dsst.forces.DSSTTesseral;
import org.orekit.time.AbsoluteDate;


public class MakeTesseral {

	/**
	 * @param args
	 * @throws OrekitException 
	 */
	public static DSSTForceModel getTesseral(Frame frame, AbsoluteDate initialDate) throws OrekitException {
		
		UnnormalizedSphericalHarmonicsProvider provider = GravityFieldFactory.getUnnormalizedProvider(10,10);
	    int maxDegreeTesseralSP = 2; // between 2 and provider.getMaxDegree()
	    
	    int maxOrderTesseralSP = 0; // between 0 and provider.getMaxOrder()
	    
	    //  Between 0 and 4
	    int maxEccPowTesseralSP  =0; 
	 
	    // Typically maxDegreeTesseralSP + maxEccPowTesseralSP and no more than 12
	    int maxFrequencyShortPeriodics = maxDegreeTesseralSP + maxEccPowTesseralSP;
	    
	    // between 2 and provider.getMaxDegree()
	    int maxDegreeMdailyTesseralSP = 3;
	    
	    // between 0 and provider.getMaxOrder()
	    int maxOrderMdailyTesseralSP =0;
	    
	    // between 0 and maxDegreeMdailyTesseralSP max(4)
	    int  maxEccPowMdailyTesseralSP = 0;//4;
	    
	 // Terre
	 			CelestialBody earth = CelestialBodyFactory.getEarth(); 
	 		    Frame earthFrame = earth.getBodyOrientedFrame();
	 		    
	    // calcul du rotation rate 
	    Vector3D rotationRate = earthFrame.getTransformTo(frame, initialDate).getRotationRate();

	    DSSTForceModel tesseralForce = new 
	    		DSSTTesseral(earthFrame, rotationRate.getNorm() ,provider,maxDegreeTesseralSP,maxOrderTesseralSP,maxEccPowTesseralSP,maxFrequencyShortPeriodics,maxDegreeMdailyTesseralSP,maxOrderMdailyTesseralSP ,maxEccPowMdailyTesseralSP );
		return tesseralForce;
	    
	}

}
