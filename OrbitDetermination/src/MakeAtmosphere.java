import org.orekit.bodies.CelestialBodyFactory;
import org.orekit.bodies.OneAxisEllipsoid;
import org.orekit.errors.OrekitException;
import org.orekit.forces.drag.Atmosphere;
import org.orekit.forces.drag.DTM2000;
import org.orekit.forces.drag.DTM2000InputParameters;
import org.orekit.forces.drag.HarrisPriester;
import org.orekit.forces.drag.MarshallSolarActivityFutureEstimation;
import org.orekit.forces.drag.MarshallSolarActivityFutureEstimation.StrengthLevel;
import org.orekit.forces.drag.SimpleExponentialAtmosphere;
import org.orekit.forces.gravity.potential.GravityFieldFactory;
import org.orekit.forces.gravity.potential.NormalizedSphericalHarmonicsProvider;
import org.orekit.frames.Frame;
import org.orekit.models.earth.EarthShape;
import org.orekit.models.earth.Geoid;
import org.orekit.models.earth.ReferenceEllipsoid;
import org.orekit.utils.Constants;
import org.orekit.utils.PVCoordinatesProvider;


public class MakeAtmosphere {

	/**
	 * @param args
	 * @throws OrekitException 
	 */
	public static Atmosphere main(double altitude) throws OrekitException {
		// TODO Auto-generated method stub
	   Frame earthFrame = CelestialBodyFactory.getEarth().getBodyOrientedFrame();
	   PVCoordinatesProvider sunPVProvider = CelestialBodyFactory.getSun();
	   ReferenceEllipsoid earthEllopsoid = 
			   new ReferenceEllipsoid(Constants.WGS84_EARTH_EQUATORIAL_RADIUS,
					   Constants.WGS84_EARTH_FLATTENING,
					   earthFrame,
					   Constants.JPL_SSD_EARTH_GM,
					   Constants.WGS84_EARTH_ANGULAR_VELOCITY);
	   
	  OneAxisEllipsoid oneAxisEllipsoid =  new ReferenceEllipsoid(Constants.WGS84_EARTH_EQUATORIAL_RADIUS,
			   Constants.WGS84_EARTH_FLATTENING,
			   earthFrame,
			   Constants.JPL_SSD_EARTH_GM,
			   Constants.GRIM5C1_EARTH_ANGULAR_VELOCITY);
		
		NormalizedSphericalHarmonicsProvider provider = 
				GravityFieldFactory.getNormalizedProvider(10, 10);
		
		EarthShape earthShape = new Geoid(provider,earthEllopsoid);
		
		DTM2000InputParameters dtmInput = new MarshallSolarActivityFutureEstimation(
				"(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\\p{Digit}\\p{Digit}\\p{Digit}\\p{Digit}F10\\.(?:txt|TXT)",
				StrengthLevel.AVERAGE);
		
		Atmosphere dtmAtmosphere = new DTM2000(dtmInput,sunPVProvider,earthShape);
		
		//http://www.braeunig.us/space/atmos.htm
		//Atmosphere seAtmosphere = MakeSimpleExponential(earthShape, altitude);
				return dtmAtmosphere;
	}
	public static  Atmosphere MakeSimpleExponential(EarthShape earthShape ,double altitude) throws OrekitException {
		Atmosphere atmosphere = null;
		if (altitude/1000 > 75 & altitude/1000 < 125){
			atmosphere =	new SimpleExponentialAtmosphere(earthShape, 5.25e-7,100000,5900);}
		else if (altitude/1000 >= 125 & altitude/1000 < 175){
			atmosphere =	new SimpleExponentialAtmosphere(earthShape, 1.73e-9,150000,25500);}
		else if (altitude/1000 >= 175 & altitude/1000 < 225){
			atmosphere =	new SimpleExponentialAtmosphere(earthShape, 2.41e-10,200000,37500);}
		else if (altitude/1000 >= 225 & altitude/1000 < 275){
			atmosphere =	new SimpleExponentialAtmosphere(earthShape, 5.97e-11,250000,44800);}
		else if (altitude/1000 >= 275 & altitude/1000 < 325){
			atmosphere =	new SimpleExponentialAtmosphere(earthShape, 1.87e-11,300000,50300);}
		else if (altitude/1000 >= 325 & altitude/1000 < 375){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape, 6.66e-12,350000,54800);}
		else if (altitude/1000 >= 375 & altitude/1000 < 425){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape,2.62e-12 ,400000,58200);}
		else if (altitude/1000 >= 425 & altitude/1000 < 475){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape, 1.09e-12,450000,61300);}
		else if (altitude/1000 >= 475 & altitude/1000 < 525){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape,  4.76e-13,500000,64500);}
		else if (altitude/1000 >= 525 & altitude/1000 < 575){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape, 2.14e-13,5500000,68700);}	
		else if (altitude/1000 >= 575 & altitude/1000 < 625){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape, 9.89e-14,600000,74800);	}	
		else if (altitude/1000 >= 625 & altitude/1000 < 675){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape, 4.73e-14,650000,84400);}	
		else if (altitude/1000 >= 675 & altitude/1000 < 725){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape, 2.36e-14,700000,99300);}		
		else if (altitude/1000 >= 725 & altitude/1000 < 775){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape, 1.24e-14,750000,121100);}		
		else if (altitude/1000 >= 775 & altitude/1000 < 825){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape, 6.95e-15,800000,151000);}		
		else if (altitude/1000 >= 825 & altitude/1000 < 875){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape, 4.22e-15,850000,188000);}		
		else if (altitude/1000 >= 875 & altitude/1000 < 925){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape, 2.78e-15,900000,226000);}		
		else if (altitude/1000 >= 925 & altitude/1000 < 975){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape, 1.98e-15,950000,263000);}		
		else if (altitude/1000 >= 975 & altitude/1000 < 1025){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape, 1.49e-15,1000000,296000);}				
		else if (altitude/1000 >= 1025 & altitude/1000 < 1375){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape, 5.70e-16,1250000,408000);}		
		else if (altitude/1000 >= 1375 & altitude/1000 < 1750){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape, 2.79e-16,1500000,516000);}		
		else if (altitude/1000 >= 1750 & altitude/1000 < 2250){
			 atmosphere =	new SimpleExponentialAtmosphere(earthShape, 9.09e-17,2000000,829000);}
		else{
			System.out.print("Altitude");
			System.out.print(altitude/1000);
			System.out.print("km is out of bound ");
		}
return atmosphere;
	}
}
