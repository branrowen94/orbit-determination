import java.io.File;

import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.hipparchus.ode.nonstiff.AdaptiveStepsizeIntegrator;
import org.hipparchus.ode.nonstiff.DormandPrince853Integrator;
import org.orekit.bodies.CelestialBody;
import org.orekit.bodies.CelestialBodyFactory;
import org.orekit.data.DataProvidersManager;
import org.orekit.data.DirectoryCrawler;
import org.orekit.errors.OrekitException;
import org.orekit.forces.gravity.potential.GravityFieldFactory;
import org.orekit.forces.gravity.potential.UnnormalizedSphericalHarmonicsProvider;
import org.orekit.frames.Frame;
import org.orekit.frames.FramesFactory;
import org.orekit.orbits.CartesianOrbit;
import org.orekit.orbits.Orbit;
import org.orekit.propagation.BoundedPropagator;
import org.orekit.propagation.SpacecraftState;
import org.orekit.propagation.semianalytical.dsst.DSSTPropagator;
import org.orekit.propagation.semianalytical.dsst.forces.DSSTForceModel;
import org.orekit.propagation.semianalytical.dsst.forces.DSSTTesseral;
import org.orekit.propagation.semianalytical.dsst.forces.DSSTZonal;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScale;
import org.orekit.time.TimeScalesFactory;
import org.orekit.utils.Constants;
import org.orekit.utils.IERSConventions;
import org.orekit.utils.PVCoordinates;
import org.orekit.utils.TimeStampedPVCoordinates;


public class PropagateDSST {

	/**
	 * @param args
	 */
	public static BoundedPropagator propagate(TimeStampedPVCoordinates initialPV,Frame frame, AbsoluteDate finalDate, double bStar, double altitude) 
			throws OrekitException {
		    AbsoluteDate initialDate = initialPV.getDate();

			double mu =  Constants.EGM96_EARTH_MU;
			Orbit initialOrbit = new CartesianOrbit(initialPV, frame, mu);
			 
		    
		    // État initial
		    double mass = 1;
		    SpacecraftState initialState = new SpacecraftState(initialOrbit,mass);
		    
		    // Tolérences de l'intégrateur
		    double minStep = 0.001;
		    double maxstep = 1000.0;
		    double positionTolerance = 10.0;
		 
		    
		    double[][] tolerances =
		    DSSTPropagator.tolerances(positionTolerance, initialOrbit);
		    
		    AdaptiveStepsizeIntegrator integrator =
		    new DormandPrince853Integrator(minStep, maxstep, tolerances[0], tolerances[1]);
		    
		    DSSTPropagator propagator = new DSSTPropagator(integrator,false);
		    DSSTForceModel zonalForce = MakeZonal.getZonal();
		    DSSTForceModel tesseralForce = MakeTesseral.getTesseral(frame, initialDate);
		    //DSSTForceModel dragForce = MakeDrag.dsst(bStar,altitude);

		    /// Force Gravitationnelle Terrestre (Tesseral)
		    
		    // Ajout de forces
		    propagator.addForceModel(zonalForce);
		    propagator.addForceModel(tesseralForce);
		   // propagator.addForceModel(dragForce);
		    propagator.setInitialState(initialState);
		    propagator.setEphemerisMode();

		    SpacecraftState finalState = propagator.propagate(finalDate);
		    BoundedPropagator ephemeris = propagator.getGeneratedEphemeris();	
		return ephemeris;

		
	}

}
