import org.apache.commons.math3.linear.MatrixUtils;
	import org.apache.commons.math3.*;
	import org.apache.commons.math3.linear.RealMatrix;
	
	
public class PredictedCovarianceMatrix {   
		
		
		public RealMatrix predicted_covariance_matrix(RealMatrix jacobian, RealMatrix covariancematrix, double h) {
			//Predicted covariance matrix
		    double q = 0.1;
		    int dimension = 6;		   
		    RealMatrix Q = MatrixUtils.createRealIdentityMatrix(dimension).scalarMultiply(q);   	    
		    RealMatrix invJ1 = jacobian.transpose();
		    RealMatrix Jacobian = jacobian.multiply(covariancematrix).add(covariancematrix.multiply(invJ1).add(Q));   
		    RealMatrix k=Jacobian.scalarMultiply(h);
		return(k);    
		}
				
	}


