import org.apache.commons.math3.linear.CholeskyDecomposition;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.*;
import org.apache.commons.math3.linear.RealMatrix;

public class Covariance_matrix {
	private RealMatrix P0;
	private RealMatrix P;
	double h = 0.1;
	private double[] t;
	private RealMatrix k1;
	private RealMatrix k2;
	private RealMatrix k3;
	private RealMatrix k4;
	public RealMatrix Jacobian1; //jacobian at time step 1
	public RealMatrix Jacobian2; //jacobian at time step 2
	public RealMatrix Jacobian3; //jacobian at time step 2 (same as jacobian 2)
	public RealMatrix Jacobian4; //jacobian at time step 4
	public RealMatrix Jacobian;
	public RealMatrix jacobian1; 
	public RealMatrix jacobian2; 
	public RealMatrix jacobian3; 
	public RealMatrix jacobian4; 
	public RealMatrix P2;
	public RealMatrix P3;
	public RealMatrix P4;
	
	
	public void Jacobian_perturbed_force(){
		double g = 9.807;
		double R_e = 6378.15;
		double mu = 398600.4415;
		double J_2 = 0.0010827;
		double J_3 = -0.0000025323;
		double J_4 = -0.0000016204;
		
		RealMatrix J = MatrixUtils.createRealIdentityMatrix(6);
		for (int i=4; i < 7; ++i){
			for (int j=1; i < 4; ++j){
				//J=J.addToEntry(int i, int i, double 1);
				//J[i j]=jacobian1.add(jacobian2);			
			}
			
		}
		
	}
	
	
	
	public void predicted_covariance_matrix() {
		//Predicted covariance matrix
	    double q = 0.1;
	    int dimension = 6;		   
	    RealMatrix Q = MatrixUtils.createRealIdentityMatrix(dimension).scalarMultiply(q);
	    
	    DecompositionSolver solution1 = new CholeskyDecomposition(jacobian1).getSolver();
	    RealMatrix invJ1 = solution1.getInverse();
	    Jacobian1 = jacobian1.multiply(P0).add(P0.multiply(invJ1).add(Q));
	    
	    DecompositionSolver solution2 = new CholeskyDecomposition(jacobian2).getSolver();
	    RealMatrix invJ2 = solution2.getInverse();	    
	    Jacobian2 = jacobian2.multiply(P2).add(P2.multiply(invJ2).add(Q));
	    
	    DecompositionSolver solution3 = new CholeskyDecomposition(jacobian3).getSolver();
	    RealMatrix invJ3 = solution3.getInverse();	    
	    Jacobian3 = jacobian3.multiply(P3).add(P3.multiply(invJ3).add(Q));
	    
	    DecompositionSolver solution4 = new CholeskyDecomposition(jacobian4).getSolver();
	    RealMatrix invJ4 = solution4.getInverse();	    
	    Jacobian4 = jacobian4.multiply(P4).add(P4.multiply(invJ4).add(Q));
	}
	
	
	public void RK4_matrix() {	
	double t0= 0.0;
	double nmax = 60.0;
	double q = 0.1;
    int dimension = 6;		   
    RealMatrix Q = MatrixUtils.createRealIdentityMatrix(dimension).scalarMultiply(q);		
	
	for (int k = 1; k < nmax; ++k) {
		DecompositionSolver solution1 = new CholeskyDecomposition(jacobian1).getSolver();
	    RealMatrix invJ1 = solution1.getInverse();
	    Jacobian1 = jacobian1.multiply(P0).add(P0.multiply(invJ1).add(Q));
		k1=Jacobian1.scalarMultiply(h);
		
		P2=P0.add(k1.scalarMultiply(0.5));
		DecompositionSolver solution2 = new CholeskyDecomposition(jacobian2).getSolver();
	    RealMatrix invJ2 = solution2.getInverse();	    
	    Jacobian2 = jacobian2.multiply(P2).add(P2.multiply(invJ2).add(Q));
	    k2=Jacobian2.scalarMultiply(h);
	    
	    P3=P0.add(k2.scalarMultiply(0.5));
	    DecompositionSolver solution3 = new CholeskyDecomposition(jacobian3).getSolver();
	    RealMatrix invJ3 = solution3.getInverse();	    
	    Jacobian3 = jacobian3.multiply(P3).add(P3.multiply(invJ3).add(Q));
	    k3=Jacobian3.scalarMultiply(h);
	    
	    P4=P0.add(k3);
	    DecompositionSolver solution4 = new CholeskyDecomposition(jacobian4).getSolver();
	    RealMatrix invJ4 = solution4.getInverse();	    
	    Jacobian4 = jacobian4.multiply(P4).add(P4.multiply(invJ4).add(Q));
	    k4=Jacobian4.scalarMultiply(h);
	    
	    P0=P0.add(k1.add((k2.add(k3).scalarMultiply(2).add(k4)))).scalarMultiply(1/6);
	    
	}    
	
	
    
	
	
	
}
}
