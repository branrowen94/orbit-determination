import java.io.IOException;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.orekit.errors.OrekitException;
import org.orekit.utils.TimeStampedPVCoordinates;

public class ExtendedKalmanFilter {
	public static TimeStampedPVCoordinates[]  main( TimeStampedPVCoordinates[] PVdata) throws OrekitException, IOException{


		//TimeStampedPVCoordinates[] PVdata = GPSData.getGPSData();
		TimeStampedPVCoordinates[] correctedPVdata = new TimeStampedPVCoordinates[PVdata.length-1];
		TimeStampedPVCoordinates propagatedData = null;
		int dimension = 6;
		RealMatrix covarianceMatrix = MatrixUtils.createRealIdentityMatrix(dimension);  
		for(int i = 0; i < PVdata.length-1; i++){
			try{
		Object[] object = Prediction.main(PVdata[i],covarianceMatrix);
		covarianceMatrix = (RealMatrix) object[0];
		propagatedData = (TimeStampedPVCoordinates) object[1];
        
		}catch(Exception e){
		propagatedData = PVdata[i+1];
		}finally{
			
		RealMatrix correctedCovarianceMatrix = Update.update(covarianceMatrix,PVdata[i+1],propagatedData);
		covarianceMatrix = correctedCovarianceMatrix;
		 RealVector stateEstimate = Update.getStateEstimate();
		 Vector3D position = new Vector3D(stateEstimate.getEntry(0), stateEstimate.getEntry(1), stateEstimate.getEntry(2) );
		 Vector3D velocity = new Vector3D(stateEstimate.getEntry(3), stateEstimate.getEntry(4), stateEstimate.getEntry(5) );
		 correctedPVdata[i] = new TimeStampedPVCoordinates(propagatedData.getDate(),position,velocity);
		}
		
		}
		System.out.println("fini!");
		return correctedPVdata;
	}
}
