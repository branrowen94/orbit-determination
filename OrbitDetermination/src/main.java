
import java.io.File;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.random.RandomGeneratorFactory;
import org.hipparchus.geometry.euclidean.threed.Rotation;
import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.hipparchus.ode.nonstiff.AdaptiveStepsizeIntegrator;
import org.hipparchus.ode.nonstiff.DormandPrince853Integrator;
import org.hipparchus.random.GaussianRandomGenerator;
import org.hipparchus.random.NormalizedRandomGenerator;
import org.hipparchus.random.RandomGenerator;
import org.hipparchus.random.UncorrelatedRandomVectorGenerator;
import org.hipparchus.random.Well19937a;
import org.hipparchus.util.FastMath;
import org.orekit.attitudes.Attitude;
import org.orekit.attitudes.AttitudeProvider;
import org.orekit.attitudes.BodyCenterPointing;
import org.orekit.attitudes.GroundPointing;
import org.orekit.bodies.BodyShape;
import org.orekit.bodies.CelestialBody;
import org.orekit.bodies.CelestialBodyFactory;
import org.orekit.bodies.Ellipsoid;
import org.orekit.data.DataProvidersManager;
import org.orekit.data.DirectoryCrawler;
import org.orekit.errors.OrekitException;
import org.orekit.forces.BoxAndSolarArraySpacecraft;
import org.orekit.forces.ForceModel;
import org.orekit.forces.gravity.HolmesFeatherstoneAttractionModel;
import org.orekit.forces.gravity.potential.GravityFieldFactory;
import org.orekit.forces.gravity.potential.NormalizedSphericalHarmonicsProvider;
import org.orekit.forces.gravity.potential.UnnormalizedSphericalHarmonicsProvider;
import org.orekit.frames.Frame;
import org.orekit.frames.FramesFactory;
import org.orekit.frames.UpdatableFrame;
import org.orekit.frames.Transform;
import org.orekit.orbits.CartesianOrbit;
import org.orekit.orbits.Orbit;
import org.orekit.orbits.OrbitType;

import org.orekit.propagation.BoundedPropagator;
import org.orekit.propagation.Propagator;
import org.orekit.propagation.SpacecraftState;
import org.orekit.propagation.analytical.KeplerianPropagator;
import org.orekit.propagation.analytical.tle.SGP4;
import org.orekit.propagation.analytical.tle.TLE;
import org.orekit.propagation.analytical.tle.TLEPropagator;
import org.orekit.propagation.numerical.NumericalPropagator;
import org.orekit.propagation.numerical.TimeDerivativesEquations;
import org.orekit.propagation.semianalytical.dsst.DSSTPropagator;
import org.orekit.propagation.semianalytical.dsst.forces.DSSTForceModel;
import org.orekit.propagation.semianalytical.dsst.forces.DSSTTesseral;
import org.orekit.propagation.semianalytical.dsst.forces.DSSTZonal;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScale;
import org.orekit.time.TimeScalesFactory;
import org.orekit.utils.PVCoordinates;
import org.orekit.utils.IERSConventions;
import org.orekit.utils.TimeStampedPVCoordinates;
import org.orekit.forces.gravity.ThirdBodyAttraction;

public class main {

	/**
	 * @param args
	 * @throws OrekitException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws OrekitException, IOException {
		File orekitData = new File("orekit-data");
		DataProvidersManager manager = DataProvidersManager.getInstance();
		manager.addProvider(new DirectoryCrawler(orekitData));
				
		//FilterTest.CompareToGPS();
		FilterTest.CompareToPropagator();

	}	
	public static void test1(double args) throws OrekitException {
		// TODO Auto-generated method stub

		
		// orekit init


		Frame satFrame = FramesFactory.getITRF(IERSConventions.IERS_2010,true);
		System.out.println(satFrame.getName());
				TimeScale utc = TimeScalesFactory.getUTC();
				TimeScale tt = TimeScalesFactory.getTT();

				// Date initiale
				AbsoluteDate initialDate = new AbsoluteDate(2016, 10, 15, 14, 44, 36.000, utc);
				
				// Masse réduite
				double mu =  3.986004415e+14;
				
				// Position initiale
				Vector3D position = new Vector3D(1521242.671, -3488092.847, 5727744.673);
				Vector3D velocity = new Vector3D(564.807906, -6477.124779, -4093.675507);
				
				
		        // Orbit initiale
				PVCoordinates pvCoordinates = new PVCoordinates(position, velocity);
				Frame inertialFrame = FramesFactory.getEME2000();
				PVCoordinates pvCoordinatesEME = satFrame.getTransformTo(inertialFrame, initialDate).transformPVCoordinates(pvCoordinates);

				
				Orbit initialOrbit = new CartesianOrbit(pvCoordinatesEME, inertialFrame, initialDate, mu);
				Orbit ConvertedOrbit = OrbitType.KEPLERIAN.convertType(initialOrbit);
		System.out.println(ConvertedOrbit);
				// Affichae Temporaire
				System.out.println(pvCoordinates);
				System.out.println(pvCoordinatesEME);
			    System.out.println(initialOrbit);
			    
			    // État initial
			    double mass = 15;
			    SpacecraftState initialState = new SpacecraftState(initialOrbit,mass);
			    

			    // Kepler Propagation
			    //Propagator kepler = new KeplerianPropagator(initialOrbit);
			    //kepler.propagate(finalDate);
			    // Adaptive step integrator
			    // with a minimum step of 0.001 and a maximum step of 1000
			    
			    
			    double minStep = 0.001;
			    double maxstep = 1000.0;
			    double positionTolerance = 10.0;
			    OrbitType propagationType = OrbitType.CARTESIAN;
			    
			    double[][] tolerances =
			    NumericalPropagator.tolerances(positionTolerance, initialOrbit, propagationType);
			    AdaptiveStepsizeIntegrator integrator =
			    new DormandPrince853Integrator(minStep, maxstep, tolerances[0], tolerances[1]);
			    
			    NumericalPropagator propagator = new NumericalPropagator(integrator);
			    propagator.setOrbitType(propagationType);
			    propagator.setEphemerisMode();

			    NormalizedSphericalHarmonicsProvider provider =
			    GravityFieldFactory.getNormalizedProvider(10, 10);
			    // Force Gravitationnelle terrestre
			    ForceModel holmesFeatherstone =
			    new HolmesFeatherstoneAttractionModel(FramesFactory.getITRF(IERSConventions.IERS_2010,true),provider);
			    //System.out.println(holmesFeatherstone.accelerationDerivatives());
			    	   
			    ForceModel sunG = new ThirdBodyAttraction(CelestialBodyFactory.getSun());
			    ForceModel mooG = new ThirdBodyAttraction(CelestialBodyFactory.getMoon());

			    // Ajout des Forces
			    propagator.addForceModel(holmesFeatherstone);
			    // propagator.addForceModel(sunG);
			   propagator.addForceModel(mooG);
			
			    propagator.setInitialState(initialState);

			    SpacecraftState finalState = propagator.propagate(initialDate.shiftedBy(12*3600));
			    BoundedPropagator ephemeris = propagator.getGeneratedEphemeris();

			   
			    String fileName = "Data.txt";
			    try {
			    	// Ouverture du fichier texte
					PrintWriter dataStream = new PrintWriter(fileName);
					dataStream.println(propagator.getAllForceModels());

			    	// initialisation
				    AbsoluteDate propagationDate = initialDate;
					Frame referentiel = FramesFactory.getTIRF(IERSConventions.IERS_2010,true);
					AbsoluteDate date = new AbsoluteDate(2017, 01, 22, 12, 45, 00.000, utc);
				    double step = 60;
				    
				    for (int itteration = 0; itteration < 12*60; itteration++){
				    	;
				    	// Date finale
				    	PVCoordinates propagatedCoordinates = ephemeris.getPVCoordinates(propagationDate, satFrame);		
				    	dataStream.println(propagatedCoordinates);
				    	
				    	// Ajout de 60s a date la de propagation
				    	propagationDate = propagationDate.shiftedBy(60);
				    }
				    dataStream.close();
			    	System.out.println("Finit test");

				} catch (FileNotFoundException e) {
			    	System.out.println("erreur d'écriture du fichier");
					e.printStackTrace();
				}
			    
			    // Référentiel terrestre
			    CelestialBody terre = CelestialBodyFactory.getEarth();

			    
			    // Génération du Jacobian
	}
	public static void tleconvert(double args) throws OrekitException {
		File orekitData = new File("/home/sasha/Documents/Orekit/Orekittesting/orekit-data");
		DataProvidersManager manager = DataProvidersManager.getInstance();
		manager.addProvider(new DirectoryCrawler(orekitData));
		
		TLE tle = new TLE("1 41602U 16040D   16289.92168475 +.00003702 +00000-0 +17285-3 0  9990"
				,"2 41602 097.4929 348.4316 0008963 204.1081 155.9735 15.20423866017589");		
	
		CelestialBody earth = CelestialBodyFactory.getEarth();
		Frame inertialFrame = earth.getInertiallyOrientedFrame();
		Frame bodyFrame = earth.getBodyOrientedFrame();
		Ellipsoid ellipsoid = new Ellipsoid(bodyFrame,1.0,1.0,1.0);
		Frame satFrame = FramesFactory.getITRF(IERSConventions.IERS_2010,false);

		AttitudeProvider attProv = new BodyCenterPointing(inertialFrame,ellipsoid);
    	System.out.println(attProv);
    	TLEPropagator tleProp = new SGP4(tle,attProv,15.00);
    	
    	TimeScale utc = TimeScalesFactory.getUTC();
    	TimeScale tt = TimeScalesFactory.getTT();
    	TimeScale gps = TimeScalesFactory.getGPS();

		// Date initiale
		AbsoluteDate tleDate = tle.getDate();
		AbsoluteDate pvDate = new AbsoluteDate(2016,10,15,22,7,36,utc);
		double offset = pvDate.timeScalesOffset(tt,utc);
		AbsoluteDate pvDate2 = new AbsoluteDate(pvDate,offset,utc);

		
		TimeStampedPVCoordinates pvCoordinatesTLE = tleProp.getPVCoordinates(pvDate2, satFrame);
        System.out.println(tleDate);
	    System.out.println(pvCoordinatesTLE);
		//6767787.792	-1113138.387	665727.2371	-968.0489679	-1356.959313	7502.989963
        // 7.36
	}
	
	public static void TestDSST(double args) throws OrekitException {
		File orekitData = new File("/home/sasha/Documents/Orekit/Orekittesting/orekit-data");
		DataProvidersManager manager = DataProvidersManager.getInstance();
		manager.addProvider(new DirectoryCrawler(orekitData));
		

		Frame satFrame = FramesFactory.getITRF(IERSConventions.IERS_2010,true);
		System.out.println(satFrame.getName());
				TimeScale utc = TimeScalesFactory.getUTC();
				
				// Date initiale
				AbsoluteDate initialDate = new AbsoluteDate(2016, 10, 15, 14, 44, 36.000, utc);
				
				// Masse réduite
				double mu =  3.986004415e+14;
				
				// Position initiale
				Vector3D position = new Vector3D(1521242.671, -3488092.847, 5727744.673);
				Vector3D velocity = new Vector3D(564.807906, -6477.124779, -4093.675507);
				
				
				
		        // Orbit initiale
				PVCoordinates pvCoordinates = new PVCoordinates(position, velocity);
				Frame inertialFrame = FramesFactory.getEME2000();
				PVCoordinates pvCoordinatesEME = satFrame.getTransformTo(inertialFrame, initialDate).transformPVCoordinates(pvCoordinates);

				
				Orbit initialOrbit = new CartesianOrbit(pvCoordinatesEME, inertialFrame, initialDate, mu);
				Orbit ConvertedOrbit = OrbitType.EQUINOCTIAL.convertType(initialOrbit);
			    
				CelestialBody earth = CelestialBodyFactory.getEarth(); 
			    Frame earthFrame = earth.getBodyOrientedFrame(); 
			    
			    // État initial
			    double mass = 15;
			    SpacecraftState initialState = new SpacecraftState(ConvertedOrbit,mass);
			    
			    double minStep = 0.001;
			    double maxstep = 100.0;
			    double positionTolerance = 10.0;
			 
			    
			    double[][] tolerances =
			    DSSTPropagator.tolerances(positionTolerance, initialOrbit);
			    
			    AdaptiveStepsizeIntegrator integrator =
			    new DormandPrince853Integrator(minStep, maxstep, tolerances[0], tolerances[1]);
			    
			    DSSTPropagator propagator = new DSSTPropagator(integrator);
			    
			    //propagator.setOrbitType(propagationType);
			    
			    //NormalizedSphericalHarmonicsProvider normalizedProvider =
					  //  GravityFieldFactory.getNormalizedProvider(10,10);
					   
			    NormalizedSphericalHarmonicsProvider provider1 =
			    GravityFieldFactory.getNormalizedProvider(10,10);
			    UnnormalizedSphericalHarmonicsProvider provider = 
			    		 GravityFieldFactory.getUnnormalizedProvider(provider1);
			    /// Force Gravitationnelle terrestre (Zonal)
			    
			    // Between 2 and provider.getMaxDegree()
			    int maxDegreeShortPeriodics = provider.getMaxDegree();
			    System.out.println(maxDegreeShortPeriodics);
			    // Between 0 and maxDegreeShortPeriodic (max 4)
			    int maxEccPowShortPeriodics  = 1;//4;
			    
			    // Between 1 and 2 * maxDegreeShortPeriodics + 1
			    int maxFrequencyShortPeriodics = 2*maxDegreeShortPeriodics+1;
				
			    // Zonal Force
			    DSSTForceModel zonalForce = new DSSTZonal(provider,maxDegreeShortPeriodics,maxEccPowShortPeriodics,maxFrequencyShortPeriodics);
			    
			    /// Force Gravitationnelle Terrestre (Tesseral)
			    provider =
					    GravityFieldFactory.getUnnormalizedProvider(10,10);
			    int maxDegreeTesseralSP = provider.getMaxDegree(); // between 2 and provider.getMaxDegree()
			    
			    int maxOrderTesseralSP = provider.getMaxOrder(); // between 0 and provider.getMaxOrder()
			    
			    //  Between 0 and 4
			    int maxEccPowTesseralSP  = 2; 
			 
			    // Typically maxDegreeTesseralSP + maxEccPowTesseralSP and no more than 12
			    maxFrequencyShortPeriodics = maxDegreeTesseralSP + maxEccPowTesseralSP;
			    
			    // between 2 and provider.getMaxDegree()
			    int maxDegreeMdailyTesseralSP = provider.getMaxDegree();
			    
			    // between 0 and provider.getMaxOrder()
			    int maxOrderMdailyTesseralSP = provider.getMaxOrder();
			    
			    // between 0 and maxDegreeMdailyTesseralSP max(4)
			    int  maxEccPowMdailyTesseralSP = 3;//4;
			    
			    // calcul du rotation rate 
			    Vector3D rotationRate = earthFrame.getTransformTo(inertialFrame, initialDate).getRotationRate();

			    DSSTForceModel tesseralForce = new 
			    		DSSTTesseral(earthFrame, rotationRate.getNorm() ,provider,maxDegreeTesseralSP,maxOrderTesseralSP,maxEccPowTesseralSP,maxFrequencyShortPeriodics,maxDegreeMdailyTesseralSP,maxOrderMdailyTesseralSP ,maxEccPowMdailyTesseralSP );
			    
			    // Ajout de forces
			    //propagator.addForceModel(zonalForce);
			    propagator.addForceModel(tesseralForce);

			    propagator.setInitialState(initialState);
			    String fileName = "Data.txt";
			    try {
			    	// Ouverture du fichier texte
					PrintWriter dataStream = new PrintWriter(fileName);
					dataStream.println("DSST-tesseral(10,10) - zonal (fromNorm)");

			    	// initialisation
				    AbsoluteDate propagationDate = initialDate;
				    
				    for (int itteration = 0; itteration < 12*60; itteration++){
				    	;
				    	// Date finale
				    	PVCoordinates propagatedCoordinates = propagator.getPVCoordinates(propagationDate, satFrame);		
				    	dataStream.println(propagatedCoordinates);
				    	// Ajout de 60s a date la de propagation
				    	propagationDate = propagationDate.shiftedBy(60);

				    }
				    dataStream.close();
			    	System.out.println("Finit test");

				} catch (FileNotFoundException e) {
			    	System.out.println("erreur d'écriture du fichier");
					e.printStackTrace();
				}
			    

}
	public static AttitudeProvider AttGen(double args) throws OrekitException {
		File orekitData = new File("/home/sasha/Documents/Orekit/Orekittesting/orekit-data");
		DataProvidersManager manager = DataProvidersManager.getInstance();
		manager.addProvider(new DirectoryCrawler(orekitData));
		
		CelestialBody earth = CelestialBodyFactory.getEarth();
		Frame inertialFrame = earth.getInertiallyOrientedFrame();
		Frame bodyFrame = earth.getBodyOrientedFrame();
		Ellipsoid ellipsoid = new Ellipsoid(bodyFrame,1.0,1.0,1.0);

		AttitudeProvider attProv = new BodyCenterPointing(inertialFrame,ellipsoid);
		return attProv;
    	
	}
	public static void SemiAnal(double args) throws OrekitException {
		File orekitData = new File("/home/sasha/Documents/Orekit/Orekittesting/orekit-data");
		DataProvidersManager manager = DataProvidersManager.getInstance();
		manager.addProvider(new DirectoryCrawler(orekitData));
		
		// référentiel temporel
		TimeScale utc = TimeScalesFactory.getUTC();
		TimeScale tt = TimeScalesFactory.getTT();

		
		// Date initiale
		AbsoluteDate initialDate = new AbsoluteDate(2016, 10, 15, 14, 44, 36.000, tt);
		
		// Position initiale
		Vector3D position = new Vector3D(1521242.671, -3488092.847, 5727744.673);
		Vector3D velocity = new Vector3D(564.807906, -6477.124779, -4093.675507);
		
		// Référentiel du sat
		Frame satFrame = FramesFactory.getITRF(IERSConventions.IERS_2010,true);
		
        // Orbit initiale
		PVCoordinates pvCoordinates = new PVCoordinates(position, velocity);
		Frame inertialFrame = FramesFactory.getEME2000();
		PVCoordinates pvCoordinatesEME = satFrame.getTransformTo(inertialFrame, initialDate).transformPVCoordinates(pvCoordinates);
		double mu =  3.986004415e+14;
		Orbit initialOrbit = new CartesianOrbit(pvCoordinatesEME, inertialFrame, initialDate, mu);
	    
		// Terre
		CelestialBody earth = CelestialBodyFactory.getEarth(); 
	    Frame earthFrame = earth.getBodyOrientedFrame(); 
	    
	    // État initial
	    double mass = 15;
	    SpacecraftState initialState = new SpacecraftState(initialOrbit,mass);
	    
	    // Tolérences de l'intégrateur
	    double minStep = 0.001;
	    double maxstep = 1000.0;
	    double positionTolerance = 10.0;
	 
	    
	    double[][] tolerances =
	    DSSTPropagator.tolerances(positionTolerance, initialOrbit);
	    
	    AdaptiveStepsizeIntegrator integrator =
	    new DormandPrince853Integrator(minStep, maxstep, tolerances[0], tolerances[1]);
	    
	    DSSTPropagator propagator = new DSSTPropagator(integrator,false);
	    propagator.setEphemerisMode();
	    UnnormalizedSphericalHarmonicsProvider provider = GravityFieldFactory.getUnnormalizedProvider(60,1);
	   
	    /// Force Gravitationnelle terrestre (Zonal)
	    
	    // Between 2 and provider.getMaxDegree()
	    int maxDegreeShortPeriodics = provider.getMaxDegree();
	    System.out.println(maxDegreeShortPeriodics);
	    // Between 0 and maxDegreeShortPeriodic (max 4)
	    int maxEccPowShortPeriodics  = 4;//4;
	    
	    // Between 1 and 2 * maxDegreeShortPeriodics + 1
	    int maxFrequencyShortPeriodics = 2*maxDegreeShortPeriodics+1;
		
	    // Zonal Force
	    DSSTForceModel zonalForce = new DSSTZonal(provider,maxDegreeShortPeriodics,maxEccPowShortPeriodics,maxFrequencyShortPeriodics);
	    
	    /// Force Gravitationnelle Terrestre (Tesseral)
	    provider =
			    GravityFieldFactory.getUnnormalizedProvider(60,60);
	    int maxDegreeTesseralSP = provider.getMaxDegree(); // between 2 and provider.getMaxDegree()
	    
	    int maxOrderTesseralSP = provider.getMaxOrder(); // between 0 and provider.getMaxOrder()
	    
	    //  Between 0 and 4
	    int maxEccPowTesseralSP  =4; 
	 
	    // Typically maxDegreeTesseralSP + maxEccPowTesseralSP and no more than 12
	    maxFrequencyShortPeriodics = 12;
	    
	    // between 2 and provider.getMaxDegree()
	    int maxDegreeMdailyTesseralSP = provider.getMaxDegree();
	    
	    // between 0 and provider.getMaxOrder()
	    int maxOrderMdailyTesseralSP = provider.getMaxOrder();
	    
	    // between 0 and maxDegreeMdailyTesseralSP max(4)
	    int  maxEccPowMdailyTesseralSP = 4;//4;
	    
	    // calcul du rotation rate 
	    Vector3D rotationRate = earthFrame.getTransformTo(inertialFrame, initialDate).getRotationRate();

	    DSSTForceModel tesseralForce = new 
	    		DSSTTesseral(earthFrame, rotationRate.getNorm() ,provider,maxDegreeTesseralSP,maxOrderTesseralSP,maxEccPowTesseralSP,maxFrequencyShortPeriodics,maxDegreeMdailyTesseralSP,maxOrderMdailyTesseralSP ,maxEccPowMdailyTesseralSP );
	    
	    // Ajout de forces
	    propagator.addForceModel(zonalForce);
	    propagator.addForceModel(tesseralForce);
	    
	    propagator.setInitialState(initialState);

	    SpacecraftState finalState = propagator.propagate(initialDate.shiftedBy(12*3600));
	    BoundedPropagator ephemeris = propagator.getGeneratedEphemeris();

	    String fileName = "Data.txt";
	    try {
	    	// Ouverture du fichier texte
			PrintWriter dataStream = new PrintWriter(fileName);
			dataStream.println("DSST-tesseral(10,10) - zonal (fromNorm)");

	    	// initialisation
		    AbsoluteDate propagationDate = initialDate;
		    
		    for (int itteration = 0; itteration < 12*60; itteration++){
		    	;
		    	// Date finale
		    	PVCoordinates propagatedCoordinates = ephemeris.getPVCoordinates(propagationDate, satFrame);		
		    	dataStream.println(propagatedCoordinates);
		    	// Ajout de 60s a date la de propagation
		    	propagationDate = propagationDate.shiftedBy(60);

		    }
		    dataStream.close();
	    	System.out.println("Finit test");

		} catch (FileNotFoundException e) {
	    	System.out.println("erreur d'écriture du fichier");
			e.printStackTrace();
		}
	  

	}
	

}