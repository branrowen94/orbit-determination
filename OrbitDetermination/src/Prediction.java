import java.util.Iterator;
import java.util.List;

import org.apache.commons.math3.analysis.function.Inverse;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.CholeskyDecomposition;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.hipparchus.analysis.differentiation.DerivativeStructure;
import org.hipparchus.geometry.euclidean.threed.FieldRotation;
import org.hipparchus.geometry.euclidean.threed.FieldVector3D;
import org.hipparchus.geometry.euclidean.threed.Rotation;
import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.orekit.errors.OrekitException;
import org.orekit.forces.ForceModel;
import org.orekit.frames.Frame;
import org.orekit.frames.FramesFactory;
import org.orekit.propagation.numerical.NumericalPropagator;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScale;
import org.orekit.time.TimeScalesFactory;
import org.orekit.utils.IERSConventions;
import org.orekit.utils.PVCoordinates;
import org.orekit.utils.TimeStampedPVCoordinates;

public class Prediction {
	public static Object[] main(TimeStampedPVCoordinates PV, RealMatrix covarianceMatrix) throws OrekitException{
		
		
		double timeStep = 60;
		 double  h = 1;
		 double nmax = timeStep/h;
		 int iteration = ((int) nmax);
		 
		Frame inertialFrame = FramesFactory.getEME2000();
		NumericalPropagator propagator = PropagateNumerical.getPropagator(PV,inertialFrame,h);

		 
		 RealMatrix[] jacobians = new RealMatrix[iteration + 2];
		 
		 for (int k = 0; k < iteration + 2; ++k){
			jacobians[k] =  new Array2DRowRealMatrix(Prediction.getJacobian(propagator));
			propagator.propagate(PV.getDate().shiftedBy(h));
			}
TimeStampedPVCoordinates propagatedPV = propagator.getPVCoordinates(PV.getDate().shiftedBy(timeStep), inertialFrame); 
		 //RealMatrix P0 = Prediction.CovarianceMatrix.getCovariance(jacobian1,jacobian2,jacobian3,jacobian4);
		 
		RealMatrix P0 = Prediction.CovarianceMatrix.getCovariance(jacobians,h,iteration,covarianceMatrix);
Object[] object= {P0,propagatedPV};
return object;
	}


	public static double[][] getJacobian(NumericalPropagator propagator) throws OrekitException{
		
		double[][] jacobian = new double[6][6] ;
		jacobian[0][0] = 0; jacobian[0][3] = 1;
		jacobian[1][0] = 0; jacobian[0][4] = 0;
		jacobian[2][0] = 0; jacobian[0][5] = 0;
		jacobian[0][1] = 0; jacobian[1][3] = 0;
		jacobian[1][1] = 0; jacobian[1][4] = 1;
		jacobian[2][1] = 0; jacobian[1][5] = 0;
		jacobian[0][2] = 0; jacobian[2][3] = 0;
		jacobian[1][2] = 0; jacobian[2][4] = 0;
		jacobian[2][2] = 0; jacobian[2][5] = 1;

		

		List<ForceModel> ForceList = propagator.getAllForceModels();
		Iterator<ForceModel> ForceItterator = ForceList.iterator();
		
		TimeStampedPVCoordinates PVs = propagator.getInitialState().getPVCoordinates(FramesFactory.getEME2000());
		
		// State parameters for Derivative Structures
		Vector3D position = PVs.getPosition();
		Vector3D velocity = PVs.getVelocity();
		Rotation rotation = propagator.getInitialState().getAttitude().getRotation();

		
		
		// DerivativeStructure Construction for acceleration derivatives computation (mass not taken)
		DerivativeStructure x = new DerivativeStructure(6, 1, 0, position.getX());
		DerivativeStructure y = new DerivativeStructure(6, 1, 1, position.getY());
		DerivativeStructure z = new DerivativeStructure(6, 1, 2, position.getZ());
		DerivativeStructure Vx = new DerivativeStructure(6, 1, 3, velocity.getX());
		DerivativeStructure Vy = new DerivativeStructure(6, 1, 4, velocity.getY());
		DerivativeStructure Vz = new DerivativeStructure(6, 1, 5, velocity.getZ());
		DerivativeStructure Q0 = new DerivativeStructure(6, 1, rotation.getQ0());
		DerivativeStructure Q1 = new DerivativeStructure(6, 1, rotation.getQ1());
		DerivativeStructure Q2 = new DerivativeStructure(6, 1, rotation.getQ2());
		DerivativeStructure Q3 = new DerivativeStructure(6, 1, rotation.getQ3());
		DerivativeStructure massDerivativeStructure = new DerivativeStructure(6, 1, propagator.getInitialState().getMass());

		// FieldVectors for acceleration derivatives
		FieldVector3D<DerivativeStructure> positionField= new FieldVector3D<DerivativeStructure>(x, y, z);
		FieldVector3D<DerivativeStructure> velocityField= new FieldVector3D<DerivativeStructure>(Vx, Vy, Vz);
		FieldRotation<DerivativeStructure> rotationField = new FieldRotation<DerivativeStructure> (Q0, Q1, Q2, Q3,true);
	
		while(ForceItterator.hasNext()){
			ForceModel force = ForceItterator.next();
			FieldVector3D<DerivativeStructure> acceleration =  force.accelerationDerivatives(PVs.getDate(),propagator.getFrame(),positionField,
					velocityField, rotationField, massDerivativeStructure);
			
			jacobian[3][0] += acceleration.getX().getPartialDerivative(1,0,0,0,0,0); 
			jacobian[4][0] += acceleration.getY().getPartialDerivative(1,0,0,0,0,0); 
			jacobian[5][0] += acceleration.getZ().getPartialDerivative(1,0,0,0,0,0); 
			jacobian[3][1] += acceleration.getX().getPartialDerivative(0,1,0,0,0,0); 
			jacobian[4][1] += acceleration.getY().getPartialDerivative(0,1,0,0,0,0); 
			jacobian[5][1] += acceleration.getZ().getPartialDerivative(0,1,0,0,0,0); 
			jacobian[3][2] += acceleration.getX().getPartialDerivative(0,0,1,0,0,0); 
			jacobian[4][2] += acceleration.getY().getPartialDerivative(0,0,1,0,0,0); 
			jacobian[5][2] += acceleration.getZ().getPartialDerivative(0,0,1,0,0,0);
			jacobian[3][3] += acceleration.getX().getPartialDerivative(0,0,0,1,0,0); 
			jacobian[4][3] += acceleration.getY().getPartialDerivative(0,0,0,1,0,0); 
			jacobian[5][3] += acceleration.getZ().getPartialDerivative(0,0,0,1,0,0);
			jacobian[3][4] += acceleration.getX().getPartialDerivative(0,0,0,0,1,0); 
			jacobian[4][4] += acceleration.getY().getPartialDerivative(0,0,0,0,1,0); 
			jacobian[5][4] += acceleration.getZ().getPartialDerivative(0,0,0,0,1,0);
			jacobian[3][5] += acceleration.getX().getPartialDerivative(0,0,0,0,0,1); 
			jacobian[4][5] += acceleration.getY().getPartialDerivative(0,0,0,0,0,1); 
			jacobian[5][5] += acceleration.getZ().getPartialDerivative(0,0,0,0,0,1);
//			System.out.print(jacobian[0][0]);System.out.print(", "); System.out.print(jacobian[0][1]); System.out.print(", "); System.out.print(jacobian[0][2]);System.out.print(", "); System.out.print(jacobian[0][3]); System.out.print(", ");System.out.print(jacobian[0][4]);System.out.print(", ");  System.out.println(jacobian[0][5]);
//			System.out.print(jacobian[1][0]); System.out.print(", ");System.out.print(jacobian[1][1]); System.out.print(", ");System.out.print( jacobian[1][2]); System.out.print(", ");System.out.print(jacobian[1][3]); System.out.print(", ");System.out.print(jacobian[1][4]);  System.out.print(", ");System.out.println(jacobian[1][5]);
//			System.out.print(jacobian[2][0]); System.out.print(", ");System.out.print(jacobian[2][1]); System.out.print(", "); System.out.print(jacobian[2][2]);System.out.print(", "); System.out.print(jacobian[2][3]);System.out.print(", ");System.out.print(jacobian[2][4]);  System.out.print(", ");System.out.println(jacobian[2][5]);
//			System.out.print(jacobian[3][0]); System.out.print(", ");System.out.print(jacobian[3][1]); System.out.print(", "); System.out.print(jacobian[3][2]); System.out.print(", ");System.out.print(jacobian[3][3]);System.out.print(", "); System.out.print(jacobian[3][4]); System.out.print(", "); System.out.println(jacobian[3][5]);
//			System.out.print(jacobian[4][0]); System.out.print(", ");System.out.print(jacobian[4][1]); System.out.print(", "); System.out.print(jacobian[4][2]); System.out.print(", ");System.out.print(jacobian[4][3]);System.out.print(", "); System.out.print(jacobian[4][4]); System.out.print(", "); System.out.println(jacobian[4][5]);
//		    System.out.print(jacobian[5][0]); System.out.print(", ");System.out.print(jacobian[5][1]); System.out.print(", ");System.out.print(jacobian[5][2]); System.out.print(", ");System.out.print(jacobian[5][3]); System.out.print(", ");System.out.print(jacobian[5][4]);  System.out.print(", ");System.out.println(jacobian[5][5]);
//		
			
		}
		
		return jacobian;

	}


public static class CovarianceMatrix {
	private static RealMatrix P0;
	private static RealMatrix P;
	private double[] t;
	private static RealMatrix k1;
	private static RealMatrix k2;
	private static RealMatrix k3;
	private static RealMatrix k4;
	public static RealMatrix Jacobian1; //jacobian at time step 1
	public static RealMatrix Jacobian2; //jacobian at time step 2
	public static RealMatrix Jacobian3; //jacobian at time step 2 (same as jacobian 2)
	public static RealMatrix Jacobian4; //jacobian at time step 4
	public RealMatrix Jacobian;
	public RealMatrix jacobian1; 
	public RealMatrix jacobian2; 
	public RealMatrix jacobian3; 
	public RealMatrix jacobian4; 
	public static  RealMatrix P2;
	public static RealMatrix P3;
	public static  RealMatrix P4;

	public static RealMatrix getCovariance(RealMatrix[] jacobians, double h,int nmax,RealMatrix P0) throws OrekitException{

		
		for (int k = 0 ; k < nmax; ++k) {
        RealMatrix jacobian1 = jacobians[k];
        RealMatrix jacobian2 = jacobians[k+1];
        RealMatrix jacobian3 = jacobians[k+2];

	    PredictedCovarianceMatrix predCovarianceMatrix = new PredictedCovarianceMatrix();
	    k1=predCovarianceMatrix.predicted_covariance_matrix(jacobian1,P0, h);	  
	    
	    P2=P0.add(k1.scalarMultiply(0.5));
	    k2 = predCovarianceMatrix.predicted_covariance_matrix(jacobian2,P2, h);
	    
	    P3=P0.add(k2.scalarMultiply(0.5));
	    k3 = predCovarianceMatrix.predicted_covariance_matrix(jacobian2,P3, h);
	    
	    P4=P0.add(k3);
	    k4 =predCovarianceMatrix.predicted_covariance_matrix(jacobian3,P4, h);    
	    
	    
	    P0 = P0.add(k1.add(((k2.add(k3).scalarMultiply(2).add(k4)))).scalarMultiply(1.0/6.0));
		}
	  return P0;
	}
}
}

