import org.orekit.errors.OrekitException;
import org.orekit.forces.gravity.potential.GravityFieldFactory;
import org.orekit.forces.gravity.potential.UnnormalizedSphericalHarmonicsProvider;
import org.orekit.propagation.semianalytical.dsst.forces.DSSTForceModel;
import org.orekit.propagation.semianalytical.dsst.forces.DSSTZonal;


public class MakeZonal {

	/**
	 * @param args
	 * @throws OrekitException 
	 */
	public static  DSSTForceModel getZonal() throws OrekitException {
		 UnnormalizedSphericalHarmonicsProvider provider = GravityFieldFactory.getUnnormalizedProvider(80,80);
		   
		    /// Force Gravitationnelle terrestre (Zonal)
		    
		    // Between 2 and provider.getMaxDegree()
		    int maxDegreeShortPeriodics = 2;
		    // Between 0 and maxDegreeShortPeriodic (max 4)
		    int maxEccPowShortPeriodics  = 0;
		    
		    // Between 1 and 2 * maxDegreeShortPeriodics + 1
		    int maxFrequencyShortPeriodics = 5;
			
		    // Zonal Force
		    DSSTForceModel zonalForce = new DSSTZonal(provider,maxDegreeShortPeriodics,maxEccPowShortPeriodics,maxFrequencyShortPeriodics);
			return zonalForce;
		   
	}

}
