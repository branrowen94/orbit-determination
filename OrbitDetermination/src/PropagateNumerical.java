import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.hipparchus.ode.ODEIntegrator;
import org.hipparchus.ode.nonstiff.AdaptiveStepsizeIntegrator;
import org.hipparchus.ode.nonstiff.ClassicalRungeKuttaIntegrator;
import org.hipparchus.ode.nonstiff.DormandPrince853Integrator;
import org.orekit.bodies.CelestialBody;
import org.orekit.bodies.CelestialBodyFactory;
import org.orekit.errors.OrekitException;
import org.orekit.forces.ForceModel;
import org.orekit.forces.drag.Atmosphere;
import org.orekit.forces.gravity.HolmesFeatherstoneAttractionModel;
import org.orekit.forces.gravity.ThirdBodyAttraction;
import org.orekit.forces.gravity.potential.GravityFieldFactory;
import org.orekit.forces.gravity.potential.NormalizedSphericalHarmonicsProvider;
import org.orekit.forces.gravity.potential.UnnormalizedSphericalHarmonicsProvider;
import org.orekit.frames.Frame;
import org.orekit.frames.FramesFactory;
import org.orekit.orbits.CartesianOrbit;
import org.orekit.orbits.Orbit;
import org.orekit.orbits.OrbitType;
import org.orekit.propagation.BoundedPropagator;
import org.orekit.propagation.SpacecraftState;
import org.orekit.propagation.numerical.NumericalPropagator;
import org.orekit.propagation.semianalytical.dsst.forces.DSSTForceModel;
import org.orekit.time.AbsoluteDate;
import org.orekit.utils.Constants;
import org.orekit.utils.IERSConventions;
import org.orekit.utils.PVCoordinates;
import org.orekit.utils.TimeStampedPVCoordinates;


public class PropagateNumerical {


		/**
		 * @param args
		 */
		public static BoundedPropagator propagate(TimeStampedPVCoordinates initialPV,Frame frame, AbsoluteDate finalDate, double bStar, double altitude) 
				throws OrekitException {

				double mu =  Constants.EGM96_EARTH_MU;
				Orbit initialOrbit = new CartesianOrbit(initialPV, frame, mu);
			
			    // État initial
			    double mass = 1;
			    SpacecraftState initialState = new SpacecraftState(initialOrbit,mass);
			    
			    // Tolérences de l'intégrateur
			    double minStep = 0.00001;
			    double maxstep = 1000.0;
			    double positionTolerance = 10.0;
			 
			    
			    double[][] tolerances =
			    NumericalPropagator.tolerances(positionTolerance, initialOrbit,OrbitType.CARTESIAN);
			    
			    AdaptiveStepsizeIntegrator integrator =
			    new DormandPrince853Integrator(minStep, maxstep, tolerances[0], tolerances[1]);
			    
			    NumericalPropagator propagator = new NumericalPropagator(integrator);
			    NormalizedSphericalHarmonicsProvider provider = GravityFieldFactory.getNormalizedProvider(5,10);
			    ForceModel holmesFeatherstone =
					    new HolmesFeatherstoneAttractionModel(FramesFactory.getITRF(IERSConventions.IERS_2010,true),provider);
			  // ForceModel dragForce = MakeDrag.numerical(bStar,altitude);
			   //ForceModel sunG = new ThirdBodyAttraction(CelestialBodyFactory.getSun());
			  //  ForceModel mooG = new ThirdBodyAttraction(CelestialBodyFactory.getMoon());

			    // Ajout de forces
			    propagator.addForceModel(holmesFeatherstone);
			   // propagator.addForceModel(dragForce);
			   // propagator.addForceModel(sunG);
			    //propagator.addForceModel(mooG);

			    propagator.setInitialState(initialState);
			    propagator.setEphemerisMode();
			    try{
			    SpacecraftState finalState = propagator.propagate(finalDate);}
			    catch(Exception e){  BoundedPropagator output = null;
			    return output;}
			    BoundedPropagator ephemeris = propagator.getGeneratedEphemeris();	
				
			   return ephemeris;

			
		}
		public static NumericalPropagator getPropagator(TimeStampedPVCoordinates initialPV,Frame frame,double step) 
				throws OrekitException {

				double mu =  Constants.EGM96_EARTH_MU;
				Orbit initialOrbit = new CartesianOrbit(initialPV, frame, mu);
			
			    // État initial
			    double mass = 1;
			    SpacecraftState initialState = new SpacecraftState(initialOrbit,mass);
			    
			    // Tolérences de l'intégrateur
			    double minStep = 0.00001;
			    double maxstep = 1000.0;
			    double positionTolerance = 10.0;
			 
			    
			    double[][] tolerances =
			    NumericalPropagator.tolerances(positionTolerance, initialOrbit,OrbitType.CARTESIAN);
			    
			   //AdaptiveStepsizeIntegrator integrator =
			   //new DormandPrince853Integrator(minStep, maxstep, tolerances[0], tolerances[1]);
			   ODEIntegrator integrator = new  ClassicalRungeKuttaIntegrator(step);
			    NumericalPropagator propagator = new NumericalPropagator(integrator);
			    NormalizedSphericalHarmonicsProvider provider = GravityFieldFactory.getNormalizedProvider(10,10);
			    ForceModel holmesFeatherstone =
					    new HolmesFeatherstoneAttractionModel(FramesFactory.getITRF(IERSConventions.IERS_2010,true),provider);
			 
			    // Ajout de forces
			    propagator.addForceModel(holmesFeatherstone);
			    propagator.setInitialState(initialState);
				
			   return propagator;

			
		}

}
