import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

import org.orekit.data.DataProvidersManager;
import org.orekit.data.DirectoryCrawler;
import org.orekit.errors.OrekitException;
import org.orekit.propagation.analytical.tle.TLE;


public class LoadTLE {

	/**
	 * @param args
	 * @throws OrekitException 
	 * @throws IOException 
	 */
	public static TLE[] getTLECollection() throws OrekitException, IOException  {
		
		String fileName = "TLECollection.txt";
        String line = null;
        int i = 0;
		
			FileReader fileReaderforCounter = new FileReader(fileName);
			// Count number of TLES
			LineNumberReader  lineReader = new LineNumberReader(fileReaderforCounter);
			lineReader.skip(Long.MAX_VALUE);
	        TLE[] tle = new TLE[lineReader.getLineNumber()/2];
			lineReader.close();
			FileReader fileReader= new FileReader(fileName);
			BufferedReader bufferedReader =  new BufferedReader(fileReader);

			while((line = bufferedReader.readLine()) != null) {
	            tle[i] = new TLE(line, bufferedReader.readLine());
	                i++;
	            }   

	            // Always close files.
	            bufferedReader.close(); 
	    		return tle;

			
		
	}
public static TLE[] getSatIDTLEs(String satID, int period) throws OrekitException, IOException  {
		
	String fileName = "TLEs"+satID+"Period"+period+".txt";
        String line = null;
        int i = 0;
        File file = new File(fileName);
        if (!file.exists()){
    		DownloadTLE.DownloadSatIDTLEs(satID, period);
        }
			FileReader fileReaderForCounter = new FileReader(fileName);
		
				
			// Counte number of TLES
			LineNumberReader  lineReader = new LineNumberReader(fileReaderForCounter);
			lineReader.skip(Long.MAX_VALUE);
	        TLE[] tle = new TLE[lineReader.getLineNumber()/2];
			lineReader.close();

	        FileReader fileReader = new FileReader(fileName);
			BufferedReader bufferedReader =  new BufferedReader(fileReader);
			
			while((line = bufferedReader.readLine()) != null) {
	            tle[i] = new TLE(line, bufferedReader.readLine());
	                i++;
	            }   

	            // Always close files.
	            bufferedReader.close(); 
			
		
		return tle;
	}

}
