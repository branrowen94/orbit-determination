import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.orekit.errors.OrekitException;
import org.orekit.frames.Frame;
import org.orekit.frames.FramesFactory;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScalesFactory;
import org.orekit.utils.IERSConventions;
import org.orekit.utils.TimeStampedPVCoordinates;

public class GPSData {
	private static TimeStampedPVCoordinates[] gpsData = null;
	
	public static void LoadGPSData() throws OrekitException, IOException  {
		//String fileName = "gps-extract-2015-10-15.csv";
		String fileName = "GPS_Extract_20170201_20170401.csv";
        String line = null;
        int i = 2;
		
       
			FileReader fileReaderforCounter = new FileReader(fileName);
			
			// Count number of TLES
			LineNumberReader  lineReader = new LineNumberReader(fileReaderforCounter);
			lineReader.skip(Long.MAX_VALUE);
	        gpsData = new TimeStampedPVCoordinates[lineReader.getLineNumber()-2];
			lineReader.close();
			
			FileReader fileReader= new FileReader(fileName);
			BufferedReader bufferedReader =  new BufferedReader(fileReader);
			bufferedReader.readLine();
			bufferedReader.readLine();
			  Frame inertialFrame = FramesFactory.getEME2000();
				//Frame satFrame = FramesFactory.getITRF(IERSConventions.IERS_2010,true);
				Frame satFrame = FramesFactory.getEME2000();

			while((line = bufferedReader.readLine()) != null) {
				String[] splitLine = line.split(",");
				Vector3D position = new Vector3D(Double.parseDouble(splitLine[6]), Double.parseDouble(splitLine[7]), Double.parseDouble(splitLine[8]));
				Vector3D velocity = new Vector3D(Double.parseDouble(splitLine[9]), Double.parseDouble(splitLine[10]), Double.parseDouble(splitLine[11]));
				String[] splitDate = splitLine[0].split(" ");
				String[] bigDate = splitDate[0].split("/");
				String[] smallDate = splitDate[1].split(":");
				AbsoluteDate date = new AbsoluteDate(Integer.parseInt(bigDate[2]),
						Integer.parseInt(bigDate[1]),Integer.parseInt(bigDate[0]),
						Integer.parseInt(smallDate[0]),Integer.parseInt(smallDate[1]),
						0.00,TimeScalesFactory.getUTC());
				TimeStampedPVCoordinates nonInertial = new TimeStampedPVCoordinates(date,position,velocity);
				gpsData[i-2] = satFrame.getTransformTo(inertialFrame, date).transformPVCoordinates(nonInertial);
				i++;

	            }   
			System.out.println(gpsData[1]);

	            bufferedReader.close(); 
	}
	public static  TimeStampedPVCoordinates[] getGPSData() throws OrekitException, IOException  {
if(gpsData == null)
	LoadGPSData();
    return gpsData;
	}
}
	
