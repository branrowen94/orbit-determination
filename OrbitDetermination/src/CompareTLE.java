import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.orekit.attitudes.AttitudeProvider;
import org.orekit.attitudes.BodyCenterPointing;
import org.orekit.attitudes.NadirPointing;
import org.orekit.bodies.CelestialBody;
import org.orekit.bodies.CelestialBodyFactory;
import org.orekit.bodies.Ellipsoid;
import org.orekit.bodies.OneAxisEllipsoid;
import org.orekit.data.DataProvidersManager;
import org.orekit.data.DirectoryCrawler;
import org.orekit.errors.OrekitException;
import org.orekit.forces.drag.Atmosphere;
import org.orekit.forces.gravity.potential.GravityFieldFactory;
import org.orekit.forces.gravity.potential.NormalizedSphericalHarmonicsProvider;
import org.orekit.frames.Frame;
import org.orekit.frames.FramesFactory;
import org.orekit.frames.Transform;
import org.orekit.models.earth.EarthShape;
import org.orekit.models.earth.Geoid;
import org.orekit.models.earth.ReferenceEllipsoid;
import org.orekit.propagation.BoundedPropagator;
import org.orekit.propagation.analytical.tle.SGP4;
import org.orekit.propagation.analytical.tle.TLE;
import org.orekit.propagation.analytical.tle.TLEPropagator;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScale;
import org.orekit.time.TimeScalesFactory;
import org.orekit.utils.Constants;
import org.orekit.utils.IERSConventions;
import org.orekit.utils.PVCoordinates;
import org.orekit.utils.TimeStampedPVCoordinates;


public class CompareTLE {

	/**
	 * @param args
	 * @throws OrekitException 
	 * @throws IOException 
	 */
	public static void CompareToCollection() throws OrekitException, IOException {
		

		// Importation des TLE de test
		TLE[] tle = LoadTLE.getTLECollection();
		String fileName = "TLECompareDSST(zonal205+tesseral3002200).txt";
	
		PrintWriter dataStream = new PrintWriter(fileName);


		CelestialBody earth = CelestialBodyFactory.getEarth();
		Frame inertialFrame = earth.getInertiallyOrientedFrame();

		Frame bodyFrame = earth.getBodyOrientedFrame();

		ReferenceEllipsoid earthEllopsoid = 
				   new ReferenceEllipsoid(Constants.WGS84_EARTH_EQUATORIAL_RADIUS,
						   Constants.WGS84_EARTH_FLATTENING,
						   bodyFrame,
						   Constants.JPL_SSD_EARTH_GM,
						   Constants.WGS84_EARTH_ANGULAR_VELOCITY);
		
			NormalizedSphericalHarmonicsProvider provider = 
					GravityFieldFactory.getNormalizedProvider(10, 10);
			
			EarthShape earthShape = new Geoid(provider,earthEllopsoid);
		AttitudeProvider attProv = new NadirPointing(inertialFrame,earthShape);
		// PV conversion
		int i = 1;
		do{	
			
			if (tle[i].getBStar()>0){
				if (tle[i].getSatelliteNumber() != tle[i-1].getSatelliteNumber()){
					System.out.println(i);
					// Conversion en PV	
					long startTime = System.nanoTime();
					TLEPropagator tleProp = new SGP4(tle[i],attProv,1);
					TimeStampedPVCoordinates pvCoordinatesTLE = tleProp.getPVCoordinates(tle[i].getDate(), inertialFrame);
					double altitude = pvCoordinatesTLE.getPosition().getNorm()-Constants.EGM96_EARTH_EQUATORIAL_RADIUS;
					BoundedPropagator boundedPropagator = PropagateDSST.propagate(pvCoordinatesTLE, inertialFrame, tle[i].getDate().shiftedBy(4*24*3600),tle[i].getBStar(),altitude);
					long endTime = System.nanoTime();
					long elapsedTime = endTime-startTime;
					int j = i+1;
					do{
try{
						// Date finale
						AbsoluteDate finalDate = tle[j].getDate();

						PVCoordinates pvCoordinatesOut = boundedPropagator.getPVCoordinates(finalDate,inertialFrame);
						Vector3D finalPropagatedPosition = pvCoordinatesOut.getPosition();
						Vector3D finalPropagatedVelocity = pvCoordinatesOut.getVelocity();
						TLEPropagator tleProp2 = new SGP4(tle[j],attProv,1);
						Vector3D finalKnownPosition = tleProp2.getPVCoordinates(finalDate, inertialFrame).getPosition();
						Vector3D finalKnownVelocity = tleProp2.getPVCoordinates(finalDate, inertialFrame).getVelocity();
						double errorOnPosition = finalKnownPosition.distance(finalPropagatedPosition);
						double errorOnVelocity = finalKnownVelocity.distance(finalPropagatedVelocity);
						double timeDifference = finalDate.durationFrom(tle[i].getDate())/3600;
						Atmosphere atmosphere = MakeAtmosphere.main(tleProp2.getPVCoordinates(finalDate, inertialFrame).getPosition().getNorm());

						dataStream.print(timeDifference);
						dataStream.print(", ");
						dataStream.print(errorOnPosition);
						dataStream.print(", ");
						dataStream.print(errorOnVelocity);
						dataStream.print(", ");
						dataStream.print(elapsedTime);
						dataStream.print(", ");
						dataStream.print(tle[i].getBStar());
						dataStream.print(", ");
						dataStream.print(tle[i].getE());
						dataStream.print(", ");
						dataStream.print(tle[i].getMeanAnomaly());
						dataStream.print(", ");
						dataStream.print(tle[i].getMeanMotion());
						dataStream.print(", ");
						dataStream.print(tle[i].getPerigeeArgument());
						dataStream.print(", ");
						dataStream.print(tle[i].getRaan());
						dataStream.print(", ");
						dataStream.print(tleProp2.getPVCoordinates(finalDate, inertialFrame).getPosition().getNorm());
						dataStream.print(", ");
						dataStream.print(tleProp2.getPVCoordinates(finalDate,FramesFactory.getITRF(IERSConventions.IERS_2010, true)).getVelocity().getNorm());
						dataStream.print(", ");
						dataStream.print(tle[i].getSatelliteNumber());
						dataStream.print(", ");
						dataStream.print(tle[j].getSatelliteNumber());
						dataStream.print(", ");
						dataStream.println(atmosphere.getDensity(finalDate, finalKnownPosition, inertialFrame));


}catch(Exception e){System.out.println("not working");}

						j++;

					}while(tle[j].getSatelliteNumber()==tle[i].getSatelliteNumber() & j+1 < tle.length);
				}
			}
			i++;
		}while(i+1 < tle.length//3700//6557
				);
		dataStream.close();
		System.out.println("Fini Comparaison");

	}
	
	public static void CompareToSingleSat(String satID, int period) throws OrekitException, IOException {
	

		// Importation des TLE de test
		TLE[] tle = LoadTLE.getSatIDTLEs(satID,period);
		String fileName = "TLECompare"+satID+".txt";
		PrintWriter dataStream = new PrintWriter(fileName);


		CelestialBody earth = CelestialBodyFactory.getEarth();
		Frame inertialFrame = earth.getInertiallyOrientedFrame();

		Frame bodyFrame = earth.getBodyOrientedFrame();

		ReferenceEllipsoid earthEllopsoid = 
				   new ReferenceEllipsoid(Constants.WGS84_EARTH_EQUATORIAL_RADIUS,
						   Constants.WGS84_EARTH_FLATTENING,
						   bodyFrame,
						   Constants.JPL_SSD_EARTH_GM,
						   Constants.WGS84_EARTH_ANGULAR_VELOCITY);
		
			NormalizedSphericalHarmonicsProvider provider = 
					GravityFieldFactory.getNormalizedProvider(10, 10);
			
			EarthShape earthShape = new Geoid(provider,earthEllopsoid);
		AttitudeProvider attProv = new NadirPointing(inertialFrame,earthShape);
		// PV conversion
		int i = 0;
		while(i+1 < tle.length){	
			
					System.out.println(i);
					// Conversion en PV	
					long startTime = System.nanoTime();
					TLEPropagator tleProp = new SGP4(tle[i],attProv,1);
					TimeStampedPVCoordinates pvCoordinatesTLE = tleProp.getPVCoordinates(tle[i].getDate(), inertialFrame);
					double altitude = pvCoordinatesTLE.getPosition().getNorm()-Constants.EGM96_EARTH_EQUATORIAL_RADIUS;
					BoundedPropagator boundedPropagator = PropagateNumerical.propagate(pvCoordinatesTLE, inertialFrame, tle[i].getDate().shiftedBy(4*24*3600),tle[i].getBStar(),altitude);
					long endTime = System.nanoTime();
					long elapsedTime = endTime-startTime;
					int j = i+1;
					while(tle[j].getDate().durationFrom(tle[i].getDate())<4*24*3600 & j+1 < tle.length){

						// Date finale
						AbsoluteDate finalDate = tle[j].getDate();

						PVCoordinates pvCoordinatesOut = boundedPropagator.getPVCoordinates(finalDate,FramesFactory.getITRF(IERSConventions.IERS_2010, true));
						Vector3D finalPropagatedPosition = pvCoordinatesOut.getPosition();
						Vector3D finalPropagatedVelocity = pvCoordinatesOut.getVelocity();
						TLEPropagator tleProp2 = new SGP4(tle[j],attProv,1);
						Vector3D finalKnownPosition = tleProp2.getPVCoordinates(finalDate, FramesFactory.getITRF(IERSConventions.IERS_2010, true)).getPosition();
						Vector3D finalKnownVelocity = tleProp2.getPVCoordinates(finalDate, FramesFactory.getITRF(IERSConventions.IERS_2010, true)).getVelocity();
						double errorOnPosition = finalKnownPosition.distance(finalPropagatedPosition);
						double errorOnVelocity = finalKnownVelocity.distance(finalPropagatedVelocity);
						double timeDifference = finalDate.durationFrom(tle[i].getDate())/3600;
						Atmosphere atmosphere = MakeAtmosphere.main(tleProp2.getPVCoordinates(finalDate, inertialFrame).getPosition().getNorm());

						dataStream.print(timeDifference);
						dataStream.print(", ");
						dataStream.print(errorOnPosition);
						dataStream.print(", ");
						dataStream.print(errorOnVelocity);
						dataStream.print(", ");
						dataStream.print(elapsedTime);
						dataStream.print(", ");
						dataStream.print(tle[i].getBStar());
						dataStream.print(", ");
						dataStream.print(tle[i].getE());
						dataStream.print(", ");
						dataStream.print(tle[i].getMeanAnomaly());
						dataStream.print(", ");
						dataStream.print(tle[i].getMeanMotion());
						dataStream.print(", ");
						dataStream.print(tle[i].getPerigeeArgument());
						dataStream.print(", ");
						dataStream.print(tle[i].getRaan());
						dataStream.print(", ");
						dataStream.print(tleProp2.getPVCoordinates(finalDate, inertialFrame).getPosition().getNorm());
						dataStream.print(", ");
						dataStream.print(tleProp2.getPVCoordinates(finalDate,FramesFactory.getITRF(IERSConventions.IERS_2010, true)).getVelocity().getNorm());
						dataStream.print(", ");
						dataStream.print(tle[i].getSatelliteNumber());
						dataStream.print(", ");
						dataStream.print(tle[j].getSatelliteNumber());
						dataStream.print(", ");
						dataStream.println(atmosphere.getDensity(finalDate, finalKnownPosition, inertialFrame));

						j++;

					};
				
			
			i++;
		}
				
		dataStream.close();
		System.out.println("Fini Comparaison");

	}

}
