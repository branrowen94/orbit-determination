

    import org.apache.commons.math3.filter.*;
	import org.apache.commons.math3.exception.DimensionMismatchException;
	import org.apache.commons.math3.exception.NullArgumentException;
	import org.apache.commons.math3.linear.Array2DRowRealMatrix;
	import org.apache.commons.math3.linear.ArrayRealVector;
	import org.apache.commons.math3.linear.CholeskyDecomposition;
	import org.apache.commons.math3.linear.DecompositionSolver;
    import org.apache.commons.math3.linear.LUDecomposition;
    import org.apache.commons.math3.linear.MatrixDimensionMismatchException;
	import org.apache.commons.math3.linear.MatrixUtils;
	import org.apache.commons.math3.linear.NonSquareMatrixException;
	import org.apache.commons.math3.linear.RealMatrix;
	import org.apache.commons.math3.linear.RealVector;
	import org.apache.commons.math3.linear.SingularMatrixException;
	import org.apache.commons.math3.util.MathUtils;
    import org.orekit.utils.TimeStampedFieldPVCoordinates;
    import org.orekit.utils.TimeStampedPVCoordinates;
	
	
public class Update {

    
    
    private static RealMatrix controlmatrix; // control matrix H
    private static RealMatrix controlmatrixT; //transposed control matrix H'
     // covariancematrix is similar to P0 in prediction
    private static RealVector  StateEstimation; // the updated state estimation y
    //private RealVector gpsdata; // Real PV coordinates at next time step
    private static RealMatrix noisematrix; // noise matrix R
    private static RealMatrix Covariancematrix;
	
	public static RealMatrix update(RealMatrix covariancematrix, TimeStampedPVCoordinates gpsData, TimeStampedPVCoordinates PVEstimate) {
	
    int dimension = 6;	// matrix dimension
    double R = -0.1; // Weighting factor
    //Creating control matrix and noise matrix
	controlmatrix = MatrixUtils.createRealIdentityMatrix(dimension);
    controlmatrixT = controlmatrix.transpose();	
    noisematrix = MatrixUtils.createRealIdentityMatrix(dimension).scalarMultiply(R);    
       
    
    // Conversion from TimeStampedPVCoordinates to RealVector
	double[] d = {gpsData.getPosition().getX(), 
				  gpsData.getPosition().getY(), 
			      gpsData.getPosition().getZ(),  
			      gpsData.getVelocity().getX(),
			      gpsData.getVelocity().getY(),
			      gpsData.getVelocity().getZ()};
	double[] d2 = {PVEstimate.getPosition().getX(), 
			PVEstimate.getPosition().getY(), 
			PVEstimate.getPosition().getZ(),  
			PVEstimate.getVelocity().getX(),
			PVEstimate.getVelocity().getY(),
			PVEstimate.getVelocity().getZ()};
	
	RealVector gpsdata = new ArrayRealVector(d);
    RealVector CurrentState = new ArrayRealVector(d2);

    
    /*
    We can check dimension mismatch
    here    
    */
    
    
    // Kalman gain is defined by:
    // K(k) = P(k)- * H' * (H * P(k)- * H' + R)^-1
	// For simplification purposes, we consider S as:
    // S = H * P(k) * H' + R
		RealMatrix s = controlmatrix.multiply(covariancematrix).multiply(controlmatrixT).add(noisematrix); 
	    DecompositionSolver solution = new LUDecomposition(s).getSolver();
	    RealMatrix invS = solution.getInverse();

    // K(k) = P(k)- * H' * S^-1	 
	    
	    
	 RealMatrix kalmanGain = covariancematrix.multiply(controlmatrixT).multiply(invS);	

	 
    //y = x + K *( z - H' * x)
	StateEstimation = CurrentState.add(kalmanGain.operate(gpsdata.subtract(controlmatrixT.operate(CurrentState))));
	
   //P = P - K * H * P
    Covariancematrix = covariancematrix.subtract(kalmanGain.multiply(controlmatrix.multiply(covariancematrix)));
    return Covariancematrix;
	}
	
	public static RealVector getStateEstimate() {
		
		return StateEstimation;
	}
}
