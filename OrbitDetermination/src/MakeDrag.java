import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.orekit.errors.OrekitException;
import org.orekit.forces.ForceModel;
import org.orekit.forces.drag.Atmosphere;
import org.orekit.forces.drag.DragForce;
import org.orekit.forces.drag.DragSensitive;
import org.orekit.forces.drag.IsotropicDrag;
import org.orekit.frames.Frame;
import org.orekit.frames.FramesFactory;
import org.orekit.propagation.semianalytical.dsst.forces.DSSTAtmosphericDrag;
import org.orekit.propagation.semianalytical.dsst.forces.DSSTForceModel;
import org.orekit.time.AbsoluteDate;
import org.orekit.utils.Constants;


public class MakeDrag {

	/**
	 * @param args
	 * @throws OrekitException 
	 */
	public static DSSTForceModel dsst(double bStar, double altitude) throws OrekitException {
		// TODO Auto-generated method stub
		Atmosphere atmosphere = MakeAtmosphere.main(altitude);
		double radii = Constants.EGM96_EARTH_EQUATORIAL_RADIUS;
		double referenceDensity =  2.461e-5*radii/1000;
		double ballisticCoefficient = 2*bStar/referenceDensity;
	

		DSSTForceModel dragForce = new DSSTAtmosphericDrag(atmosphere,1.00,ballisticCoefficient);
		
		return dragForce;
	}
	
	public static ForceModel numerical(double bStar, double altitude) throws OrekitException {
		// TODO Auto-generated method stub
		Atmosphere atmosphere = MakeAtmosphere.main(altitude);
		double radii = Constants.EGM96_EARTH_EQUATORIAL_RADIUS;
		double referenceDensity =  2.461e-5;
		//double ballisticCoefficient = 2*bStar*radii/referenceDensity;
		double ballisticCoefficient = bStar/radii;

	System.out.println(ballisticCoefficient);
		DragSensitive isotropic = new IsotropicDrag(1,ballisticCoefficient);
		ForceModel dragForce = new DragForce(atmosphere,isotropic);
		
		return dragForce;
	}

}
