import java.io.IOException;
import java.io.PrintWriter;

import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.hipparchus.random.GaussianRandomGenerator;
import org.hipparchus.random.UncorrelatedRandomVectorGenerator;
import org.hipparchus.random.Well19937a;
import org.orekit.errors.OrekitException;
import org.orekit.frames.Frame;
import org.orekit.frames.FramesFactory;
import org.orekit.propagation.BoundedPropagator;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScale;
import org.orekit.time.TimeScalesFactory;
import org.orekit.utils.IERSConventions;
import org.orekit.utils.PVCoordinates;
import org.orekit.utils.TimeStampedPVCoordinates;

public class FilterTest {
	public static void  CompareToPropagator() throws OrekitException, IOException{
	Vector3D position = new Vector3D(1521242.671, -3488092.847, 5727744.673);
	Vector3D velocity = new Vector3D(564.807906, -6477.124779, -4093.675507);
	
	
    // Orbit initiale
	PVCoordinates pvCoordinates = new PVCoordinates(position, velocity);
    Frame inertialFrame = FramesFactory.getEME2000();
	Frame satFrame = FramesFactory.getITRF(IERSConventions.IERS_2010,true);
	TimeScale utc = TimeScalesFactory.getUTC();

	// Date initiale
	AbsoluteDate initialDate = new AbsoluteDate(2016, 10, 15, 14, 44, 36.000, utc);
	
	
	//TimeStampedPVCoordinates[] pvData =  ExtendedKalmanFilter.main();
	int days = 1;

	//TimeStampedPVCoordinates[] pvData = GPSData.getGPSData();
    PVCoordinates pvCoordinatesEME = satFrame.getTransformTo(inertialFrame, initialDate).transformPVCoordinates(pvCoordinates);
	TimeStampedPVCoordinates PV =new TimeStampedPVCoordinates(initialDate, pvCoordinatesEME);
	BoundedPropagator boundedPropagator = PropagateDSST.propagate(PV, inertialFrame,initialDate.shiftedBy(3600*24*days) ,1.0,1.0);
 
	TimeStampedPVCoordinates[] propagatedPV = new TimeStampedPVCoordinates[60*24*days];
	 Vector3D[] perturbedPosition = new Vector3D[60*24*30];
	 Vector3D[] perturbedVelocity = new Vector3D[60*24*30];
	TimeStampedPVCoordinates[] perturbedPV = new TimeStampedPVCoordinates[60*24*days];
	 
	Well19937a Ngen = new Well19937a();
	 GaussianRandomGenerator rawGenerator = new GaussianRandomGenerator(Ngen);
	 UncorrelatedRandomVectorGenerator Ugen = new UncorrelatedRandomVectorGenerator(6,rawGenerator);
	 double multiplicator = 0;

	for(int i = 0; i < 60*24*days; i++){	
	 propagatedPV[i] = boundedPropagator.getPVCoordinates(initialDate.shiftedBy(i*60), inertialFrame);
	 
	 if(Ngen.nextDouble()<0.01){
		 multiplicator = 3000;
	 }else if(Ngen.nextDouble()<0.1){
		 multiplicator = 1;
	 }else{
		 multiplicator = 0;
	 }
	 perturbedPosition[i] = new Vector3D(propagatedPV[i].getPosition().getX()+(rawGenerator.nextNormalizedDouble()-0.5)*multiplicator,
			 propagatedPV[i].getPosition().getY()+(rawGenerator.nextNormalizedDouble()-0.5)*multiplicator,
			 propagatedPV[i].getPosition().getZ()+(rawGenerator.nextNormalizedDouble()-0.5)*multiplicator);
			
	 
	 perturbedVelocity[i] = new Vector3D( propagatedPV[i].getVelocity().getX()+(rawGenerator.nextNormalizedDouble()-0.5)*multiplicator,
			 propagatedPV[i].getVelocity().getY()+(rawGenerator.nextNormalizedDouble()-0.5)*multiplicator,
			 propagatedPV[i].getVelocity().getZ()+(rawGenerator.nextNormalizedDouble()-0.5)*multiplicator);
	 perturbedPV[i] = new TimeStampedPVCoordinates(initialDate,perturbedPosition[i],perturbedVelocity[i]);
	}
	
	TimeStampedPVCoordinates[] filteredData = ExtendedKalmanFilter.main(perturbedPV);
	
	String fileName = "KalmanFilterCaract.txt";
	PrintWriter dataStream = new PrintWriter(fileName);
	for(int i =0; i< propagatedPV.length-1; i++){
		dataStream.print(filteredData[i].getPosition().getX());
		dataStream.print(", ");
		dataStream.print(filteredData[i].getPosition().getY());
		dataStream.print(", ");
		dataStream.print(filteredData[i].getPosition().getZ());
		dataStream.print(", ");
		dataStream.print(filteredData[i].getVelocity().getX());
		dataStream.print(", ");
		dataStream.print(filteredData[i].getVelocity().getY());
		dataStream.print(", ");
		dataStream.print(filteredData[i].getVelocity().getZ());
		dataStream.print(", ");
		dataStream.print(perturbedPV[i].getPosition().getX());
		dataStream.print(", ");
		dataStream.print(perturbedPV[i].getPosition().getY());
		dataStream.print(", ");
		dataStream.print(perturbedPV[i].getPosition().getZ());
		dataStream.print(", ");
		dataStream.print(perturbedPV[i].getVelocity().getX());
		dataStream.print(", ");
		dataStream.print(perturbedPV[i].getVelocity().getY());
		dataStream.print(", ");
		dataStream.print(perturbedPV[i].getVelocity().getZ());
		dataStream.print(", ");
		dataStream.print(propagatedPV[i].getPosition().getX());
	dataStream.print(", ");
	dataStream.print(propagatedPV[i].getPosition().getY());
	dataStream.print(", ");
	dataStream.print(propagatedPV[i].getPosition().getZ());
	dataStream.print(", ");
	dataStream.print(propagatedPV[i].getVelocity().getX());
	dataStream.print(", ");
	dataStream.print(propagatedPV[i].getVelocity().getY());
	dataStream.print(", ");
	dataStream.println(propagatedPV[i].getVelocity().getZ());}
	dataStream.close();
	System.out.println("fini!");
	}
	
	public static void  CompareToGPS() throws OrekitException, IOException{
	
	
	
	TimeStampedPVCoordinates[] propagatedPV2 = GPSData.getGPSData();
	TimeStampedPVCoordinates[] propagatedPV1 = new TimeStampedPVCoordinates[5000];
for (int i = 0; i<5000;i++){
	propagatedPV1[i] = propagatedPV2[i];
}
	
	
	
	Well19937a Ngen = new Well19937a();
	 GaussianRandomGenerator rawGenerator = new GaussianRandomGenerator(Ngen);
	 UncorrelatedRandomVectorGenerator Ugen = new UncorrelatedRandomVectorGenerator(6,rawGenerator);
	 double multiplicator = 0.1;
	 TimeStampedPVCoordinates[] propagatedPV = ExtendedKalmanFilter.main(propagatedPV1);
	 Vector3D[] perturbedPosition = new Vector3D[propagatedPV.length];
	 Vector3D[] perturbedVelocity = new Vector3D[propagatedPV.length];
	TimeStampedPVCoordinates[] perturbedPV = new TimeStampedPVCoordinates[propagatedPV.length];
	for(int i = 0; i < propagatedPV.length; i++){	
	 
	 
		 if(i%1==0){

			 perturbedPosition[i] = new Vector3D(propagatedPV[i].getPosition().getX()+(rawGenerator.nextNormalizedDouble()-0.5)*multiplicator,
					 propagatedPV[i].getPosition().getY()+(rawGenerator.nextNormalizedDouble()-0.5)*multiplicator,
					 propagatedPV[i].getPosition().getZ()+(rawGenerator.nextNormalizedDouble()-0.5)*multiplicator);
					
			 
			 perturbedVelocity[i] = new Vector3D( propagatedPV[i].getVelocity().getX()+Ngen.nextDouble()+(rawGenerator.nextNormalizedDouble()-0.5)*multiplicator,
					 propagatedPV[i].getVelocity().getY()+(rawGenerator.nextNormalizedDouble()-0.5)*multiplicator,
					 propagatedPV[i].getVelocity().getZ()+(rawGenerator.nextNormalizedDouble()-0.5)*multiplicator);
			 perturbedPV[i] = new TimeStampedPVCoordinates(propagatedPV[i].getDate(),perturbedPosition[i],perturbedVelocity[i]);
			 }else{ perturbedPV[i] = propagatedPV[i];}}
	
	TimeStampedPVCoordinates[] filteredData = ExtendedKalmanFilter.main(perturbedPV);
	
	String fileName = "KalmanFilterCaractGPS.txt";
	PrintWriter dataStream = new PrintWriter(fileName);
	for(int i =0; i< propagatedPV.length-1; i++){
		dataStream.print(filteredData[i].getPosition().getX());
		dataStream.print(", ");
		dataStream.print(filteredData[i].getPosition().getY());
		dataStream.print(", ");
		dataStream.print(filteredData[i].getPosition().getZ());
		dataStream.print(", ");
		dataStream.print(filteredData[i].getVelocity().getX());
		dataStream.print(", ");
		dataStream.print(filteredData[i].getVelocity().getY());
		dataStream.print(", ");
		dataStream.print(filteredData[i].getVelocity().getZ());
		dataStream.print(", ");
		dataStream.print(perturbedPV[i].getPosition().getX());
		dataStream.print(", ");
		dataStream.print(perturbedPV[i].getPosition().getY());
		dataStream.print(", ");
		dataStream.print(perturbedPV[i].getPosition().getZ());
		dataStream.print(", ");
		dataStream.print(perturbedPV[i].getVelocity().getX());
		dataStream.print(", ");
		dataStream.print(perturbedPV[i].getVelocity().getY());
		dataStream.print(", ");
		dataStream.print(perturbedPV[i].getVelocity().getZ());
		dataStream.print(", ");
		dataStream.print(propagatedPV[i].getPosition().getX());
	dataStream.print(", ");
	dataStream.print(propagatedPV[i].getPosition().getY());
	dataStream.print(", ");
	dataStream.print(propagatedPV[i].getPosition().getZ());
	dataStream.print(", ");
	dataStream.print(propagatedPV[i].getVelocity().getX());
	dataStream.print(", ");
	dataStream.print(propagatedPV[i].getVelocity().getY());
	dataStream.print(", ");
	dataStream.println(propagatedPV[i].getVelocity().getZ());}
	dataStream.close();
	System.out.println("fini!");
	}
}
